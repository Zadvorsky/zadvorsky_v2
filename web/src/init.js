(function() {
    window.site = {
        // backbone
        routers:{},
        views:{},
        models:{},
        collections:{},
        // custom stuff
        utils:{},
        // globals and/or hacks
        globals:{},
        // controlled by dat.gui, used throughout the site
        settings:{
            minFrequency:1200,
            deltaFrequency:1500,
            detune:-1600,
            stagger:0.05,
            outDuration:0.25,
            inDuration:1,
            easeParam0:1.5,
            easeParam1:0.5
        }
    };
})();