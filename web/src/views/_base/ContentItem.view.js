site.views.ContentItem = Backbone.View.extend({
    events:{
        'click .toggle-button' : 'toggleButtonHandler'
    },
    collapsedHeight:370,
    buttonPadding:48,

    initialize:function() {
        TweenMax.set(this.$el, {height:this.collapsedHeight + 'px'});

        this.collapsed = true;
    },
    toggleButtonHandler:function(e) {
        e.preventDefault();

        var tl = new TimelineMax();

        if (this.collapsed) {
            tl.set(this.$el, {height:'auto'});
            tl.from(this.$el, 0.25, {height:this.collapsedHeight + 'px', ease:Power2.easeIn});
            tl.to(this.$el, 0.25, {paddingBottom:this.buttonPadding + 'px', ease:Power2.easeOut});
            tl.to(this.$('.toggle-button'), 0.5, {rotation:180, ease:Power2.easeInOut}, 0);
        }
        else {
            tl.to(this.$el, 0.25, {paddingBottom:0, ease:Power2.easeIn});
            tl.to(this.$el, 0.25, {height:this.collapsedHeight + 'px', ease:Power2.easeOut});
            tl.to(this.$('.toggle-button'), 0.5, {rotation:0, ease:Power2.easeInOut}, 0);
        }

        this.collapsed = !this.collapsed;
    },
    extend:function(child) {
        var view = Backbone.View.extend.apply(this, arguments);

        view.prototype.events = _.extend({}, this.prototype.events, child.events);

        return view;
    }
});
