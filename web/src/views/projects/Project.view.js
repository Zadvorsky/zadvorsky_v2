site.views.Project = site.views.ContentItem.extend({
    tagName:'article',
    className:'projects item',
    template: _.template($('#project-view-template').html()),

    render:function() {
        this.$el.html(this.template(this.model.attributes));

        if (!this.model.attributes.banner) {
            this.$('.banner').remove();
        }

        return this;
    }
});
