site.models.Pen = Backbone.Model.extend({});
site.collections.Pens = Backbone.Collection.extend({
    model: site.models.Pen,
    url: 'data/pens.json'
});
