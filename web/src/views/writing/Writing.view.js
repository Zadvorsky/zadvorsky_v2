site.views.Writing = site.views.Content.extend({
    el: $('#writing-view-template').html(),

    initialize:function() {
        this.$section = this.$('.article-container');

        this.collection = new site.collections.Writing();
        this.listenTo(this.collection, 'reset', this.renderArticles);
        this.collection.fetch({reset:true});
    },
    renderArticles:function() {
        this.collection.each(function(model) {
            var view = new site.views.Article({model:model});
            this.$section.append(view.render().el);
        }, this);

        this.loadComplete();
    }
});