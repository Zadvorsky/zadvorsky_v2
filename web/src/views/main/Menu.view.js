site.views.Menu = Backbone.View.extend({
    el:$('#menu-view'),

    initialize:function() {
        this.$closeButton = this.$el.find('.close-button');
        this.closeButtonClasses = ['projects', 'lab', 'writing', 'about'];

        // todo: maybe change this based on enquire
        this.lineSpacing = 120;

        this.initMenuCanvas();
        this.initMenuItems();
        this.initPointerTracker();

        if (site.globals.audioContext) {
            site.events.on('gui.change', this.applyAudioSettings, this);
            site.events.on('gui.animate', this.animateMenuItems, this);

            this.applyAudioSettings();
        }

        site.events.on('tick', _.bind(this.update, this));
        site.events.on('resize', _.bind(this.resize, this));

        this.disable();
    },
    initMenuCanvas:function() {
        this.menuCanvas = new site.views.Canvas({el:'#menu-canvas'});
    },
    initMenuItems:function() {
        var $menuButtons = this.$el.find('.main-menu .border-button');
        var hues = [10, 160, 200, 270];

        this.menuItems = [];

        // create a MenuItem view for each button
        for (var i = 0; i < $menuButtons.length; i++) {
            var item = new site.views.MenuItem({el:$menuButtons[i]});

            item.color.h = hues[i];

            if (site.globals.audioContext) {
                item.initAudioNodes();
            }

            this.menuItems.push(item);
        }
    },
    applyAudioSettings:function() {
        var item;

        for (var i = 0; i < this.menuItems.length; i++) {
            item = this.menuItems[i];
            item.oscillatorNode.frequency.value = site.settings.minFrequency + site.settings.deltaFrequency * i;
            item.oscillatorNode.detune.value = site.settings.detune;
        }
    },
    initPointerTracker:function() {
        this.pointerTracker = new site.utils.PointerTracker(this.menuCanvas.el, this.pointerMoveHandler, this);
    },
    animateMenuItems:function() {
        // fake a mouse interaction by defining a velocity and direction to stretch the items to
        var tl = new TimelineMax({onComplete:this.enable, onCompleteScope:this}),
            velocity = 100,
            direction = new site.math.Point(0, 1), // down
            stagger = site.settings.stagger;

        this.menuItems.forEach(function(item, index) {
            var t = item.stretch(direction, velocity);
            // animations can clash if the user has set a very long animation duration
            if (t) {
                tl.add(t, stagger * index);
            }
        });

        return tl;
    },
    enable:function() {
        _.invoke(this.menuItems, 'enable');
        this.pointerTracker.enabled = true;
    },
    disable:function() {
        _.invoke(this.menuItems, 'disable');
        this.pointerTracker.enabled = false;
    },
    pointerMoveHandler:function() {
        // check for intersection between each item and the line between the current and last pointer position
        this.menuItems.forEach(function(line) {
            var intersection = site.math.intersect(line.p0, line.p1, this.pointerTracker.p0, this.pointerTracker.p1);

            // if an intersection is found, stretch the line based on the direction and velocity of the pointer
            if (intersection.intersects) {
                line.stretch(this.pointerTracker.direction, this.pointerTracker.velocity);
            }
        }, this);
    },
    update:function() {
        this.menuCanvas.clear();

        this.menuItems.forEach(function(line) {
            line.render(this.menuCanvas.ctx);
        }, this);
    },
    setContentState:function(activeIndex) {
        // save active index so we animate back later
        this.activeIndex = activeIndex;

        // set close button class to match the content
        this.$closeButton.removeClass(this.closeButtonClasses.join(' '));
        this.$closeButton.addClass(this.closeButtonClasses[activeIndex]);

        // no more interaction :(
        this.disable();

        var tl = new TimelineMax(),
            activeDelay = 0.3;

        // move the active index to the top, hide the others
        for (var i = 0; i < this.menuItems.length; i++) {
            if (i === activeIndex) {
                tl.add(this.menuItems[i].animateTop(site.globals.menuContentStateTop), activeDelay);
            }
            else {
                tl.add(this.menuItems[i].hide(), 0)
            }
        }

        return tl;
    },
    setMenuState:function() {
        var tl = new TimelineMax(),
            targetTop = (this.lineSpacing * this.activeIndex) + this.menuTop;

        // return active time to its correct position
        tl.add(this.menuItems[this.activeIndex].animateTop(targetTop));
        tl.add('hide-active-complete');

        // show the other buttons
        for (var i = 0; i < this.menuItems.length; i++) {
            if (i !== this.activeIndex) {
                tl.add(this.menuItems[i].show(), 'hide-active-complete-=0.2');
            }
        }
        // enable after the animation is done
        tl.call(this.enable, null, this);

        // reset active index
        this.activeIndex = -1;

        return tl;
    },
    showCloseButton:function() {
        var tl = new TimelineMax();

        tl.set(this.$closeButton, {autoAlpha:1});
        tl.fromTo(this.$closeButton, 0.4, {y:-120}, {y:0, ease:Power3.easeOut});

        return tl;
    },
    hideCloseButton:function() {
        var tl = new TimelineMax();

        tl.fromTo(this.$closeButton, 0.4, {y:0}, {y:-120, ease:Power3.easeIn});
        tl.set(this.$closeButton, {autoAlpha:0});

        return tl;
    },
    resize:function() {
        // center the menu vertically
        this.menuHeight = (this.menuItems.length - 1) * this.lineSpacing;
        this.menuTop = (this.el.clientHeight - this.menuHeight) * 0.5;

        for (var i = 0; i < this.menuItems.length; i++) {
            var line = this.menuItems[i];

            // reposition the active item at the top (if any)
            if (i === this.activeIndex) {
                line.top = site.globals.menuContentStateTop;
            }
            // adjust item spacing
            else {
                line.top = this.menuTop + this.lineSpacing * i;
            }

            line.width = this.el.clientWidth;
            line.resize();
        }
    }
});
