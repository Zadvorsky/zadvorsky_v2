site.views.Projects = site.views.Content.extend({
    el: $('#projects-view-template').html(),

    initialize:function() {
        this.$section = this.$('.article-container');

        this.collection = new site.collections.Projects();
        this.listenTo(this.collection, 'reset', this.renderProjects);
        this.collection.fetch({reset:true});
    },
    renderProjects:function() {
        this.collection.each(function(model) {
            var view = new site.views.Project({model:model});
            this.$section.append(view.render().el);
        }, this);

        this.loadComplete();
    }
});