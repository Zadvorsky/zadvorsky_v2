site.views.Content = Backbone.View.extend({
    events:{
        'click .back-button' : 'backButtonHandler'
    },
    loaded:false,
    backButtonHandler:function(e) {
        e.preventDefault();

        TweenMax.to(window, 0.75, {scrollTo:0, ease:Power1.easeInOut, onComplete:function() {
            window.location.hash = '';
        }});
    },
    show:function() {
        return TweenMax.fromTo(this.el, 0.5, {y:100, alpha:0}, {y:0, alpha:1, ease:Power3.easeOut, onComplete:this.showComplete, onCompleteScope:this});
    },
    hide:function() {
        return TweenMax.fromTo(this.el, 0.5, {y:0, alpha:1}, {y:50, alpha:0, ease:Power3.easeIn, onComplete:this.hideComplete, onCompleteScope:this});
    },
    loadComplete:function() {
        this.loaded = true;

        site.events.trigger('content.loaded');
    },
    showComplete:function() {

    },
    hideComplete:function() {

    },
    detach:function() {
        this.$el.detach();
    },
    extend:function(child) {
        var view = Backbone.View.extend.apply(this, arguments);

        view.prototype.events = _.extend({}, this.prototype.events, child.events);

        return view;
    }
});
