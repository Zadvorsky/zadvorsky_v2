// todo: make actually useful
site.utils.ImageLoader = function() {

};
site.utils.ImageLoader.prototype = {
    load:function(url, completeCallback) {
        this.img = document.createElement('img');

        this.img.onload = completeCallback;
        this.img.src = url;
    }
};