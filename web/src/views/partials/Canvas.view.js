site.views.Canvas = Backbone.View.extend({
    tagName:'canvas',

    initialize:function(options) {
        this.ctx = this.el.getContext('2d');
        this.ctx.save();
        this.pixelRatio = window.devicePixelRatio;
        
        this.resize = _.bind(this.resize, this);
        this.autoResize((options.autoResize !== false));
    },
    autoResize:function(v) {
        if (v === undefined) return this._autoResize;

        this._autoResize = v;

        if (v) {
            site.events.on('resize', this.resize);
            this.resize();
        }
        else {
            site.events.off('resize', this.resize);
        }
    },
    clear:function() {
        this.ctx.clearRect(0, 0, this.el.width, this.el.height);
    },
    resize:function(w, h) {
        this.width = this.el.width = (w || this.el.clientWidth) * this.pixelRatio;
        this.height = this.el.height = (h || this.el.clientHeight) * this.pixelRatio;
        
        this.ctx.restore();
        this.ctx.scale(this.pixelRatio, this.pixelRatio);
        this.ctx.save();
    }
});
