site.math = {};

site.math.intersect = (function() {
    var result = {
        intersects:false,
        x:0,
        y:0
    };

    return function(a1, a2, b1, b2) {
        var ua_t = (b2.x - b1.x) * (a1.y - b1.y) - (b2.y - b1.y) * (a1.x - b1.x);
        var ub_t = (a2.x - a1.x) * (a1.y - b1.y) - (a2.y - a1.y) * (a1.x - b1.x);
        var u_b  = (b2.y - b1.y) * (a2.x - a1.x) - (b2.x - b1.x) * (a2.y - a1.y);

        result.intersects = false;
        result.x = 0;
        result.y = 0;

        if ( u_b != 0 ) {
            var ua = ua_t / u_b;
            var ub = ub_t / u_b;

            if ( 0 <= ua && ua <= 1 && 0 <= ub && ub <= 1 ) {
                result.intersects = true;
                result.x = a1.x + ua * (a2.x - a1.x);
                result.y = a1.y + ua * (a2.y - a1.y);
            }
        }

        return result;
    };
})();

site.math.cubicBezier = (function() {
    var result = {
        x:0,
        y:0
    };

    return function(p0, c0, c1, p1, t) {
        var nt = (1 - t);

        result.x = nt * nt * nt * p0.x + 3 * nt * nt * t * c0.x + 3 * nt * t * t * c1.x + t * t * t * p1.x;
        result.y = nt * nt * nt * p0.y + 3 * nt * nt * t * c0.y + 3 * nt * t * t * c1.y + t * t * t * p1.y;

        return result;
    };
})();

site.math.clamp = function(x, min, max) {
    return x < min ? min : x > max ? max : x;
};

site.math.map = function(s, a1, a2, b1, b2) {
    return ((s - a1)/(a2 - a1)) * (b2 - b1) + b1
};

site.math.wrap = function(x, min, max) {
    return (((x - min) % (max - min)) + (max - min)) % (max - min) + min;
};

// EASING
// t = time
// b = begin value
// c = change in value
// d = duration

site.math.easeInQuad = function(t, b, c, d) {
    return c*(t/=d)*t + b;
};

site.math.easeOutQuad = function(t, b, c, d) {
    return -c *(t/=d)*(t-2) + b;
};

site.math.easeInCubic = function(t, b, c, d) {
    return c*(t/=d)*t*t + b;
};

site.math.easeOutQuart = function(t, b, c, d) {
    return -c * ((t=t/d-1)*t*t*t - 1) + b;
};

site.math.Point = function(x, y) {
    this.x = x || 0;
    this.y = y || 0;
};
site.math.Point.prototype = {
    sub:function(p) {
        this.x -= p.x;
        this.y -= p.y;

        return this;
    },
    copy:function(p) {
        this.x = p.x;
        this.y = p.y;

        return this;
    },
    length:function() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    },
    normalize:function() {
        var l = this.length();

        this.x /= l;
        this.y /= l;

        return this;
    }
};

site.math.Range = function(min, max) {
    this.min = min;
    this.max = max;
};
site.math.Range.random = function(min, max) {
    return min + (max - min) * Math.random();
};
site.math.Range.clamp = function(x, min, max) {
    return x < min ? min : x > max ? max : x;
};
site.math.Range.wrap = function(x, min, max) {
    return (((x - min) % (max - min)) + (max - min)) % (max - min) + min;
};
site.math.Range.map = function(s, a1, a2, b1, b2) {
    return ((s - a1)/(a2 - a1)) * (b2 - b1) + b1
};
site.math.Range.prototype = {
    constructor:site.math.Range,
    random:function() {
        return this.constructor.random.call(this, this.min, this.max);
    },
    clamp:function(x) {
        return this.constructor.clamp.call(this, x, this.min, this.max);
    },
    wrap:function(x) {
        return this.constructor.wrap.call(this, x, this.min, this.max);
    },
    map:function(x, other) {
        return this.constructor.map.call(this, x, this.min, this.max, other.min, other.max);
    }
};


// not really math
site.math.Color = function(h, s, l, a) {
    this.h = h;
    this.s = s;
    this.l = l;
    this.a = a;
};
site.math.Color.prototype = {
    toString:function() {
        // abs lightness because it gets tweened below zero in elastic tweens
        var l = this.l < 0 ? -this.l : this.l;

        return 'hsla(' + this.h + ',' + this.s + '%,' + l + '%,' + this.a + ')';
    }
};