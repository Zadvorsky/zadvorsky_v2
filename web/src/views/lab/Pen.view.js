site.views.Pen = site.views.ContentItem.extend({
    tagName:'article',
    className:'lab item',
    template: _.template($('#pen-view-template').html()),

    collapsedHeight:420,

    render:function() {
        this.$el.html(this.template(this.model.attributes));
        this.$iframe = this.$('iframe');
        this.iframeLoaded = false;

        return this;
    },
    showComplete:function() {
        site.events.on('scroll', this.scrollHandler, this);
        site.events.on('resize', this.resizeHandler, this);

        this.loadOrUnloadIframe();
    },
    hideComplete:function() {
        site.events.off('scroll', this.scrollHandler, this);
        site.events.off('resize', this.resizeHandler, this);

        this.unloadIFrame();
    },
    scrollHandler:function() {
        this.loadOrUnloadIframe();
    },
    resizeHandler:function() {
        this.loadOrUnloadIframe();
    },
    loadOrUnloadIframe:function() {
        // iframe should load on tablet and up
        // iframe should only load when it is in view
        if (this.iframeLoaded === true && site.globals.windowWidthState < 3) {
            this.unloadIFrame();
        }
        else if (this.iframeLoaded === false && site.globals.windowWidthState >= 3) {
            if (this.$el.offset().top - site.$window.scrollTop() < window.innerHeight) {
                this.loadIFrame();
            }
        }
    },
    loadIFrame:function() {
        this.$iframe.attr('src', this.formatIframeUrl(this.model.attributes.hash));
        this.iframeLoaded = true;
    },
    unloadIFrame:function() {
        this.$iframe.attr('src', '');
        this.iframeLoaded = false;
    },
    formatIframeUrl:function(hash) {
        return '//codepen.io/zadvorsky/embed/' + hash + '/?height=350&theme-id=13864'
    }
});
