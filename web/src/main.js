(function () {
    window.site.init = function() {
        // jq cache
        site.$document = $(document);
        site.$window = $(window);

        // events
        site.events = _.clone(Backbone.Events);

        site.$window.on('resize', function() {
            site.events.trigger('resize');
        });

        site.$window.on('scroll', function() {
            site.events.trigger('scroll');
        });
        // update/draw in harmony with TweenMax
        TweenMax.ticker.addEventListener("tick", function() {
            site.events.trigger('tick');
        });

        // try creating the webAudio context
        try {
            site.globals.audioContext = new (window.AudioContext || window.webkitAudioContext)();
        }
        catch(e) {
            site.globals.audioContext = false;
        }

        // init main views and router (router must go last because it fires events to the Main view
        // do these need a reference?
        new site.views.Loader();
        new site.views.Main();
        new site.routers.Main();

        // hide loader after inits
        site.events.trigger('loader.hide');
    };

    $(window.site.init);
}());

// for testing
//window.onerror = function(msg, url, line) {
//    alert('error! url: ' + url + ' | line: ' + line + ' | msg: ' + msg);
//};