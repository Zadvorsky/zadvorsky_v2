var gulp = require('gulp');
var rev = require('gulp-rev');
var del = require('del');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var compass = require('gulp-compass');

/* Folder paths */
var publicPath = '../web/';
var bowerPath = publicPath + '_bower/';
var jsBuildPath = publicPath + 'build/';

/* Files to watch and build */
var javascriptFiles = [
    bowerPath + 'jquery/dist/jquery.js',
    bowerPath + 'underscore/underscore.js',
    bowerPath + 'backbone/backbone.js',
    bowerPath + 'enquire/dist/enquire.js',
    bowerPath + 'gsap/src/uncompressed/TweenMax.js',
    bowerPath + 'gsap/src/uncompressed/plugins/scrollToPlugin.js',
    bowerPath + 'gsap/src/uncompressed/utils/Draggable.js',
    bowerPath + 'dat-gui/build/dat.gui.js',
    publicPath + 'src/init.js',
    publicPath + 'src/main.js',
    publicPath + 'src/views/_base/*.js',
    publicPath + 'src/**/*.js'
];

/**
 * Clean the build folder, as preperation for next build
 */
gulp.task('clean', function() {
    del([
        jsBuildPath + '/**'
    ], {force: true});
});

/**
 * Build for development, with sourcemaps and without uglify
 */
gulp.task('build-development', function() {
    return gulp.src(javascriptFiles)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('final.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(jsBuildPath));
});

/**
 * Build for development, without sourcemaps and with uglify
 */
gulp.task('build-production', function() {
    return gulp.src(javascriptFiles)
        .pipe(plumber())
        .pipe(uglify())
        .pipe(concat('final.js'))
        .pipe(gulp.dest(jsBuildPath))
});

/**
 * Watcher for development. Cleans build folder before and does a build before starting watching
 */
gulp.task('default', ['clean'], function() {
    gulp.start('build-development');
    gulp.watch(javascriptFiles, ['build-development']);
});







///* Sass Files to watch and build */
//var sassFiles = [
//    publicPath + 'scss/**/*.scss'
//];

/**
 * Build for production, without sourcemaps and uglified
 * Runs clean before running this
 */
//gulp.task('build-production', ['clean'], function() {
//    return gulp.src(javascriptFiles)
//        .pipe(uglify())
//        .pipe(concat('final.min.js'))
//        .pipe(rev())
//        .pipe(gulp.dest(jsBuildPath))
//        .pipe(rev.manifest())
//        .pipe(gulp.dest(jsBuildPath));
//});

/**
 * Build for development, with sourcemaps and without uglify
 */
//gulp.task('build-development', function() {
//    return gulp.src(javascriptFiles)
//        .pipe(plumber())
//        .pipe(sourcemaps.init())
//        .pipe(concat('final.js'))
//        .pipe(sourcemaps.write())
//        .pipe(rev())
//        .pipe(gulp.dest(jsBuildPath))
//        .pipe(rev.manifest())
//        .pipe(gulp.dest(jsBuildPath));
//});

/**
 * Build for compass
 */
//gulp.task('build-compass', function() {
//    return gulp.src(sassFiles)
//        .pipe(compass({
//            config_file: publicPath + './config.rb',
//            css: publicPath + 'css',
//            sass: publicPath + 'sass'
//        }))
//        .pipe(gulp.dest(publicPath + 'css'));
//});

/**
 * Shorthand for production build task
 */
//gulp.task('build', ['build-production']);

///**
// * Watcher for compass
// * @TODO add this to default task (currently not working on every PC?)
// * @TODO move config.rb to _scripts folder for consistency
// */
//gulp.task('compass', function() {
//    gulp.start('build-compass');
//    gulp.watch(sassFiles, ['build-compass']);
//});

