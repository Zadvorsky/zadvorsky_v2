site.views.Main = Backbone.View.extend({
    el:$('#main-view'),

    initialize:function() {
        this.$contentContainer = this.$('#content-container');

        this.menuView = new site.views.Menu();
        this.contentView = null;
        this.viewCache = {};

        if (site.globals.audioContext) {
            this.guiView = new site.views.GUI();
        }
        else {
            this.$('.gui-container').remove();
        }

        site.events.on('router.showMainMenu', this.showMainMenu, this);
        site.events.on('router.showProjectsView', this.showProjectsView, this);
        site.events.on('router.showWritingView', this.showWritingView, this);
        site.events.on('router.showLabView', this.showLabView, this);
        site.events.on('router.showAboutView', this.showAboutView, this);

        this.enquire();
    },
    getContentView:function(Class) {
        if (!this.viewCache[Class]) {
            this.viewCache[Class] = new site.views[Class]();
        }
        return this.viewCache[Class];
    },
    showProjectsView:function() {
        this.contentView = this.getContentView('Projects');
        this.showContentView(0);
    },
    showLabView:function() {
        this.contentView = this.getContentView('Lab');
        this.showContentView(1);
    },
    showWritingView:function() {
        this.contentView = this.getContentView('Writing');
        this.showContentView(2);
    },
    showAboutView:function() {
        this.contentView = this.getContentView('About');
        this.showContentView(3);
    },
    showContentView:function(activeIndex) {
        this.$contentContainer.html(this.contentView.el);

        var tl = new TimelineMax();
        // show main in case of deeplink
        tl.set(this.el, {autoAlpha:1});
        // hide gui if present so it's not in the way
        if (this.guiView) {
            tl.add(this.guiView.hide());
        }
        // animate the menu to the correct state
        tl.add(this.menuView.setContentState(activeIndex), 0);
        // if the content is already loaded
        if (this.contentView.loaded) {
            // show it overlapping the menu animation
            tl.add(this.contentView.show(), '-=0.5');
            tl.add(this.menuView.showCloseButton(), '-=0.5');
        }
        // if the content is not yet loaded
        else {
            // pause the tween after the menu animation
            tl.addPause('+=0', function() {
                // if the content has been loaded during the animation
                if (this.contentView.loaded) {
                    // continue to the content animation
                    tl.resume();
                }
                // STILL NOT LOADED!?!?
                else {
                    // wait for event and show the preloader so the user doesn't get bored
                    site.events.trigger('loader.show');
                    site.events.once('content.loaded', tl.resume, tl);
                }
            }, null, this);
            // hide the loader (has no effect if the loader was not shown)
            tl.addCallback(function() {
                site.events.trigger('loader.hide');
            });
            // show the content view and close button
            tl.add(this.contentView.show());
            tl.add(this.menuView.showCloseButton(),'-=0.5');
        }
        // if there was a deeplink, skip ahead to the end
        // if the content isn't loaded, the timeline will still be paused
        if (site.globals.fromDeeplink) {
            tl.progress(1);
            site.globals.fromDeeplink = false;
        }
    },
    showMainMenu:function() {
        var tl = new TimelineMax();
        // this is the back to main menu animation
        if (this.contentView) {
            tl.add(this.menuView.hideCloseButton());
            tl.add(this.contentView.hide(), '-=0.5');
            // remove the content from the dom, but don't destroy it
            tl.call(_.bind(this.contentView.detach, this.contentView));
            tl.add(this.menuView.setMenuState());
        }
        // this is the intro animation if you enter from the main menu
        else {
            tl.to(this.el, 0.5, {autoAlpha:1}, 0.5); // small delay for performance
            tl.add(this.menuView.animateMenuItems());
        }
        // if the gui is present, show it last in both cases
        if (this.guiView) {
            tl.add(this.guiView.show());
        }
    },
    enquire:function() {
        // [min, max] ranges for responsive states
        var small = [0, 40],
            medium = [40.063, 50],
            large = [50.063, 90];

        enquire.register("(max-width: " + small[1] + "em)", {
            match:function() {
                site.globals.windowWidthState = 1;
                site.globals.menuContentStateTop = 32;
                site.events.trigger('resize');
            }
        });
        enquire.register("(min-width: " + medium[0] + "em) and (max-width: " + medium[1] + 'em)', {
            match:function() {
                site.globals.windowWidthState = 2;
                site.globals.menuContentStateTop = 48;
                site.events.trigger('resize');
            }
        });
        enquire.register("(min-width: " + large[0] + "em)", {
            match:function() {
                site.globals.windowWidthState = 3;
                site.globals.menuContentStateTop = 80;
                site.events.trigger('resize');
            }
        });
    }
});
