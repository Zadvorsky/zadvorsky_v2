site.views.MenuItem = Backbone.View.extend({
    initialize:function() {
        // anchor the element (button) at it's center
        this.el.style.marginLeft = Math.floor(this.el.clientWidth * -0.5) + 'px';
        this.el.style.marginTop = Math.floor(this.el.clientHeight * -0.5) + 'px';

        // total width of the line (set in resize)
        this.width = 0;
        // y position of the line (set in resize)
        this.top = 0;
        // p0, p1 and control are used to draw bezier curves
        this.p0 = new site.math.Point();
        this.p1 = new site.math.Point();
        this.control = new site.math.Point();
        // control point will return to anchor point after stretching
        this.anchor = new site.math.Point();
        // hls color
        this.color = new site.math.Color(0, 75, 0, 1);
        this.maxLightness = 60;
        // if individual tones are too loud, the sound is bad
        this.maxVolume = 0.2;
        this.dragDistance = 0;
        // lock is true while the element is being dragged by Draggable
        this.locked = false;

        site.events.on('gui.change', this.updateEaseParams, this);
        this.updateEaseParams();

        this.makeDraggable();
    },
    updateEaseParams:function() {
        this.elasticParams = [site.settings.easeParam0, site.settings.easeParam1];
    },
    makeDraggable:function() {
        // is this the kind of code you want me to write, javascript?
        this.draggable = Draggable.create(this.el, {
            type:'x,y',
            cursor:'pointer',
            dragClickables:true,
            minimumMovement:4,
            // reset all the things
            onDragStart:function(e) {
                TweenMax.killTweensOf(this.el);
                TweenMax.killTweensOf(this.control);
                TweenMax.killTweensOf(this.color);

                if (this.gainNode) {
                    this.gainNode.gain.value = 0;
                    TweenMax.killTweensOf(this.gainNode.gain);
                }

                this.locked = true;
            },
            onDragStartScope:this,
            // cancel drag if dragged too far
            onDrag:function(e) {
                var x = this.el._gsTransform.x - this.anchor.x,
                    y = this.el._gsTransform.y - this.anchor.y,
                    scale = 1.3;

                this.dragDistance = Math.sqrt(x * x + y * y);
                this.control.x = this.anchor.x + x * scale;
                this.control.y = this.anchor.y + y * scale;
                this.color.l = site.math.easeOutQuart(this.dragDistance, 0, this.maxLightness, this.maxDragDistance);

                if (this.dragDistance > this.maxDragDistance) {
                    this.draggable.endDrag(e);
                }
            },
            onDragScope:this,
            // snap back to anchor if user stops drag, or drags too far
            // also make sound if possible
            onDragEnd:function() {
                this.snapBackTween = new TimelineMax({
                    onComplete:function() {
                        this.locked = false;
                    },
                    onCompleteScope:this
                });

                this.snapBackTween.to(this.el, site.settings.inDuration, {x:this.anchor.x, y:this.anchor.y, ease:Elastic.easeOut, easeParams:this.easeParams}, 0);
                this.snapBackTween.to(this.control, site.settings.inDuration, {x:this.anchor.x, y:this.anchor.y, ease:Elastic.easeOut, easeParams:this.easeParams}, 0);
                this.snapBackTween.to(this.color, site.settings.inDuration, {l:0, ease:Elastic.easeOut, easeParams:this.easeParams}, 0);

                if (this.gainNode) {
                    var volume = site.math.easeInQuad(this.dragDistance, 0, 0.15, this.maxDragDistance);
                    this.snapBackTween.fromTo(this.gainNode.gain, site.settings.inDuration, {value:volume}, {value:0, ease:Elastic.easeOut, easeParams:this.easeParams}, 0);
                }
            },
            onDragEndScope:this
        })[0];
    },
    initAudioNodes:function() {
        this.oscillatorNode = site.globals.audioContext.createOscillator();
        this.oscillatorNode.type = 'sine';
        this.oscillatorNode.start(0); // the 0 argument is required on iOS (everything will break down if it is omitted)

        this.gainNode = site.globals.audioContext.createGain();
        this.gainNode.gain.value = 0;

        this.oscillatorNode.connect(this.gainNode);
        this.gainNode.connect(site.globals.audioContext.destination);
    },
    makeClickSound:function() {
        return TweenMax.fromTo(this.gainNode.gain, 1, {value:0.15}, {value:0, ease:Elastic.easeOut});
    },
    resize:function() {
        this.p0.x = 0;
        this.p0.y = this.top;
        this.p1.x = this.width;
        this.p1.y = this.top;
        this.anchor.x = this.control.x = this.width * 0.5;
        this.anchor.y = this.control.y = this.top;

        this.maxDragDistance = window.innerHeight * 0.25;
        this.maxPointerVelocity = window.innerHeight * 0.20;
    },
    stretch:function(direction, velocity) {
        if (this.locked) return null;

        var minVelocity = 1,
            maxVelocity = this.maxPointerVelocity;

        velocity = site.math.clamp(velocity, minVelocity, maxVelocity);

        var tl = new TimelineMax(),
            targetX = this.anchor.x + direction.x * velocity,
            targetY = this.anchor.y + direction.y * velocity,
            targetLightness = site.math.easeOutQuart(velocity, 0, this.maxLightness, maxVelocity),
            outDuration = site.settings.outDuration,
            inDuration = site.settings.inDuration;
        // first move the control point in the direction of the movement
        // then bounce back to the anchor (the center)
        tl.to(this.control, outDuration, {x:targetX, y:targetY, ease:Cubic.easeOut}, 0);
        tl.to(this.control, inDuration, {x:this.anchor.x, y:this.anchor.y, ease:Elastic.easeOut, easeParams:this.elasticParams}, outDuration);
        // change the hue based on the velocity of the movement
        tl.to(this.color, outDuration, {l:targetLightness, ease:Cubic.easeOut}, 0);
        tl.to(this.color, inDuration, {l:0, ease:Elastic.easeOut, easeParams:this.elasticParams}, outDuration);
        // make the sound
        if (this.gainNode) {
            var volume = site.math.easeInQuad(velocity, 0, this.maxVolume, maxVelocity);

            tl.to(this.gainNode.gain, outDuration, {value:volume, ease:Cubic.easeOut}, 0);
            tl.to(this.gainNode.gain, inDuration, {value:0, ease:Elastic.easeOut, easeParams:this.elasticParams}, outDuration);
        }

        return tl;
    },
    animateTop:function(top) {
        var tl = new TimelineMax(),
            distance = Math.abs(this.anchor.y - top),
            speed = 600,
            duration = Math.max(0.6, distance / speed);

        this.anchor.y = top;

        tl.to(this.p0, duration * 0.5, {y:top, ease:Cubic.easeInOut}, 0);
        tl.to(this.p1, duration * 0.5, {y:top, ease:Cubic.easeInOut}, 0);
        tl.to(this.control, duration, {y:top, ease:Elastic.easeOut}, duration * 0.25);

        return tl;
    },
    show:function() {
        // for some reason, tl.set(this.el, {autoAlpha:1})
        // gets called immediately as the whole timeline starts (tweenmax bug?)
        // to circumvent this, we set the autoAlpha onStart (which is timed correctly)
        var tl = new TimelineMax({
            onStart:function(){
                TweenMax.set(this.el, {autoAlpha:1});
            },
            onStartScope:this
        });

        tl.to(this.color, 0.25, {a:1}, 0);
        tl.to(this.el, 0.25, {color:'#000000'}, 0);

        return tl;
    },
    hide:function() {
        var tl = new TimelineMax();

        // animate the color instead of the alpha to prevent the canvas line from showing through the element
        tl.to(this.color, 0.25, {a:0}, 0);
        tl.to(this.el, 0.25, {color:'#f1f1f1'}, 0);
        tl.set(this.el, {autoAlpha:0});

        return tl;
    },
    enable:function() {
        this.$el.toggleClass('disabled', false);
        this.draggable.enable();
    },
    disable:function() {
        this.$el.toggleClass('disabled', true);
        this.draggable.disable();
    },
    render:function(ctx) {
        ctx.fillStyle = ctx.strokeStyle = this.color.toString(); // toString() isn't implicit on ios?
        ctx.lineWidth = 2;
        // draw a cubic bezier using this.control twice because it looks nicer than a quadratic one
        ctx.beginPath();
        ctx.moveTo(this.p0.x, this.p0.y);
        ctx.bezierCurveTo(this.control.x, this.control.y, this.control.x, this.control.y, this.p1.x, this.p1.y);
        ctx.stroke();
        // the dots on the sides
        var size = 8,
            halfSize = size * 0.5;

        ctx.fillRect(this.p0.x, this.p0.y - halfSize, size, size);
        ctx.fillRect(this.p1.x - size, this.p1.y - halfSize, size, size);

        // todo: this doesn't seem to work in IE11 (and lower?)
        this.el.style.outlineColor = this.color;

        // get the point halfway the bezier curve
        var p = site.math.cubicBezier(this.p0, this.control, this.control, this.p1, 0.5);
        // floor the position to prevent font anti-alias issues
        TweenMax.set(this.el, {x:p.x | 0, y:p.y | 0});
    }
});
