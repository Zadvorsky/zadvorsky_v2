site.views.Article = site.views.ContentItem.extend({
    tagName:'article',
    className:'writing item',
    template: _.template($('#article-view-template').html()),

    render:function() {
        this.$el.html(this.template(this.model.attributes));

        // todo: find better way to do this (template framework?)
        var links = this.model.attributes.links,
            formattedLink;

        // if more than one link, make a list after the paragraph
        if (links.length > 1) {
            var $list = $('<ul>');

            this.$('p').append($list);

            for (var i = 0; i < links.length; i++) {
                formattedLink = this.formatLink(links[i]);
                $list.append('<li>' + formattedLink + '</li>');
            }
        }
        // if only one link, add to subtitle
        else {
            formattedLink = this.formatLink(links[0]);
            this.$('small').append(' | ' + formattedLink);
        }

        return this;
    },
    formatLink:function(link) {
        return '<a target="_blank" href="' + link.url + '">' + link.text + '</a>';
    }
});
