site.views.About = site.views.Content.extend({
    el: $('#about-view-template').html(),

    initialize:function() {
        // settings for the drawing
        this.settings = {
            width:400,
            height:400,
            threshold:160,
            sampleScale:0.5,
            maxWeight:12,
            frameStep:20,
            lineWidth:0.1
        };

        this.initTitle();
        this.initCanvas();

        this.imageLoader = new site.utils.ImageLoader();
        this.imageLoader.load('img/me.jpg', _.bind(this.imageLoadedHandler, this));
    },
    initTitle:function() {
        // is this too cheesy?
        var $title = this.$('h1'),
            date = new Date(),
            hours = date.getHours(),
            greeting = '';

        switch (true) {
            case hours < 6:
                greeting = 'evening!'; // night?
                break;
            case hours < 12:
                greeting = 'morning!';
                break;
            case hours < 18:
                greeting = 'afternoon!';
                break;
            default :
                greeting = 'evening!';
                break;
        }

        $title.html('Good ' + greeting);
    },
    initCanvas:function() {
        this.canvas = new site.views.Canvas({el:this.$('canvas')});
        this.canvas.ctx.save();

        // off-screen canvas
        this.osCanvas = new site.views.Canvas({autoResize:false});
        this.osCanvas.resize(this.settings.width, this.settings.height);
    },
    imageLoadedHandler:function() {
        this.loadComplete();
    },
    showComplete:function() {
        this.canvas.resize();
        this.processImage();
        this.resize();

        site.events.on('tick', this.update, this);
        site.events.on('resize', this.resize, this);
    },
    hideComplete:function() {
        this.osCanvas.clear();
        this.canvas.clear();

        site.events.off('tick', this.update, this);
        site.events.off('resize', this.resize, this);
    },
    // todo make image processing framework
    processImage:function() {

        //console.log('processing...');

        //var t0 = Date.now();
        var points = this.getPointsFromImage(this.imageLoader.img);
        //var t1 = Date.now();

        //console.log('points:', points.length);
        //console.log('step 1 took:', (t1 - t0));

        this.generatePathFromPoints(points);

        //var t2 = Date.now();

        //console.log('step 2: took', (t2 - t1));

        this.osCanvas.clear();
        this.frame = 0;
    },
    /**
     * get {x, y} points from pixels with a brightness above a threshold
     * @param image
     * @returns {Array}
     */
    getPointsFromImage:function(image) {
        var threshold = this.settings.threshold,
            sampleScale = this.settings.sampleScale,
            maxWeight = this.settings.maxWeight,
            infSampleScale = 1 / sampleScale,
            sampleSize = (this.osCanvas.height * sampleScale) | 0;

        this.osCanvas.ctx.drawImage(image, 0, 0, sampleSize, sampleSize);

        var imageData = this.osCanvas.ctx.getImageData(0, 0, sampleSize, sampleSize),
            pixels = imageData.data,
            points = [];

        for (var i = 0; i < pixels.length; i += 4) {
            var r = pixels[i    ],
                g = pixels[i + 1],
                b = pixels[i + 2],
                avg = ((r + g + b) / 3) | 0,
                x = ((i / 4) % sampleSize) * infSampleScale,
                y = ((i / 4 / sampleSize) | 0) * infSampleScale;

            if (avg > threshold) {
                var p = {
                    x:x + site.math.Range.random(-0.1, 0.1),
                    y:y + site.math.Range.random(-0.1, 0.1),
                    weight:site.math.Range.map(avg, threshold, 255, 1, maxWeight) | 0
                };

                points.push(p);
            }
        }

        return points;
    },
    /**
     * build a k-d tree from {x, y} points
     * generate path from the tree through nearest neighbor search
     * @param points
     * @returns {Array}
     */
    generatePathFromPoints:function(points) {
        var settings = this.settings;
        var random = Math.random;

        var distance = function(a, b) {
                var dx = (a.x - b.x) * (random() - 0.5);
                var dy = (a.y - b.y) * (random() - 0.5);
                // no need to sqrt because the exact distance does not matter
                return dx * dx + dy * dy;
            },
            dims = ['x', 'y'];

        var pathTree = new kdTree(points, distance, dims),
            copyTree = new kdTree(points, distance, dims),
            path = [],
            point = points[0],
            length = points.length,
            next;

        while (length) {
            next = pathTree.nearest(point, 1)[0][0];
            point = next;

            pathTree.remove(point);
            path.push(point);

            length--;
        }

        this.path = path;
        this.tree = copyTree;
    },

    update:function() {
        this.updateDrawing();
        this.drawToCanvas();
    },
    updateDrawing:function() {
        var point = this.path[this.frame],
            step = 0,
            steps = this.settings.frameStep;

        var neighbors;

        var h = (this.frame % 360),
            s = 80,
            l = 0;

        this.osCanvas.ctx.lineWidth = this.settings.lineWidth;

        this.osCanvas.ctx.strokeStyle = 'hsl(' + h + ',' + s + '%,' + l + '%)';

        while ((++step <= steps) && point) {

            this.osCanvas.ctx.beginPath();
            this.osCanvas.ctx.moveTo(point.x, point.y);

            neighbors = this.tree.nearest(point, point.weight);

            neighbors.forEach(function(n){
                this.osCanvas.ctx.lineTo(n[0].x, n[0].y);
            }, this);

            this.osCanvas.ctx.stroke();

            point = this.path[++this.frame];
        }
    },
    drawToCanvas:function() {
        // this.canvas.clear();
        this.canvas.ctx.clearRect(-1000, 0, 3000, this.canvas.height);
        this.canvas.ctx.drawImage(this.osCanvas.el, 0, 0, this.settings.width, this.settings.height);
    },
    resize:function() {
        this.canvas.ctx.restore();
        this.canvas.ctx.translate((this.canvas.width - this.settings.width) * 0.5, 0);
    }
});