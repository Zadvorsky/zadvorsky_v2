site.views.GUI = Backbone.View.extend({
    el:'.gui-container',
    events:{
        'click .settings-button' : 'toggleOpen'
    },

    initialize:function() {
        this.gui = new dat.GUI();
        this.gui
            .add(site.settings, 'minFrequency', 100, 10000)
            .step(1)
            .name('min frequency')
            .onChange(_.bind(this.dispatchChange, this));

        this.gui
            .add(site.settings, 'deltaFrequency', 100, 2000)
            .step(1)
            .name('delta frequency')
            .onChange(_.bind(this.dispatchChange, this));

        this.gui
            .add(site.settings, 'detune', -5000, 5000)
            .step(1)
            .name('detune')
            .onChange(_.bind(this.dispatchChange, this));

        this.gui
            .add(site.settings, 'stagger', 0, 1)
            .name('stagger');

        this.gui
            .add(site.settings, 'outDuration', 0.1, 5)
            .name('out duration');

        this.gui
            .add(site.settings, 'inDuration', 0.1, 5)
            .name('back duration');

        this.gui
            .add(site.settings, 'easeParam0', 0.01, 5)
            .name('ease amplitude')
            .onChange(_.bind(this.dispatchChange, this));

        this.gui
            .add(site.settings, 'easeParam1', 0.01, 5)
            .name('ease period')
            .onChange(_.bind(this.dispatchChange, this));

        this.gui
            .add(this, 'dispatchAnimate')
            .name('animate!');

        this.gui.width = 289;

        this.$el.append(this.gui.domElement);
    },
    dispatchChange:function() {
        site.events.trigger('gui.change');
    },
    dispatchAnimate:function() {
        site.events.trigger('gui.animate');
    },
    toggleOpen:function(e) {
        e.preventDefault();

        this.$el.toggleClass('gui-container-open');
    },
    show:function() {
        return TweenMax.fromTo(this.el, 0.25, {autoAlpha:0}, {autoAlpha:1});
    },
    hide:function() {
        return TweenMax.fromTo(this.el, 0.25, {autoAlpha:1}, {autoAlpha:0});
    }
});
