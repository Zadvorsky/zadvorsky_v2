site.models.Article = Backbone.Model.extend({});
site.collections.Writing = Backbone.Collection.extend({
    model: site.models.Article,
    url: 'data/writing.json'
});
