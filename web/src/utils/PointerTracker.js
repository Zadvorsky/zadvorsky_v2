site.utils.PointerTracker = function(el, moveCallback, context) {
    this.el = el;
    this.callback = _.bind(moveCallback, context);

    // infinity to make sure the point
    this.p0 = new site.math.Point(Infinity, Infinity);
    this.p1 = new site.math.Point();
    this.direction = new site.math.Point();
    this.velocity = 0;
    this.enabled = true;

    site.$window.blur(_.bind(this.reset, this));
    site.$window.on('touchend touchstart', _.bind(this.reset, this));
    site.$window.on('mousemove', _.bind(this.mouseMoveHandler, this));
    site.$window.on('touchmove', _.bind(this.touchMoveHandler, this));

    // this is a hack for iOS which mutes the audio context until a sound is triggered in a user input event
    // the sound in the touchStartHandler is muted through the gain node
    // doing this enables all the other sounds to play! YAY!
    // this should probably be cleaned up a bit...
    if (site.globals.audioContext) {
        this._iOSHackTouchStartHandler = _.bind(this.iOSHackTouchStartHandler, this);
        window.addEventListener('touchstart', this._iOSHackTouchStartHandler);
    }
};
site.utils.PointerTracker.prototype = {
    iOSHackTouchStartHandler:function(e) {
        // go go go!
        var g = site.globals.audioContext.createGain();
        var o = site.globals.audioContext.createOscillator();
        g.gain.value = 0;
        o.connect(g);
        g.connect(site.globals.audioContext.destination);
        o.start(0);

        setTimeout(function() {
            g.disconnect(site.globals.audioContext.destination);
            o.disconnect(g);
            o.stop(0);
        }, 100);

        window.removeEventListener('touchstart', this._iOSHackTouchStartHandler);
    },
    touchMoveHandler:function(e) {
        if (this.enabled) e.preventDefault();

        var touch = e.originalEvent.touches[0];

        this.update(touch.pageX, touch.pageY);
    },
    mouseMoveHandler:function(e) {
        this.update(e.clientX, e.clientY);
    },
    update:function(clientX, clientY) {
        var r = this.el.getBoundingClientRect();

        this.p0.x = this.p1.x;
        this.p0.y = this.p1.y;
        this.p1.x = clientX - r.left;
        this.p1.y = clientY - r.top;

        if (this.enabled === false) return;

        this.direction.x = this.p1.x - this.p0.x;
        this.direction.y = this.p1.y - this.p0.y;
        this.velocity = this.direction.length();

        // don't divide by zero
        if (this.velocity !== 0) {
            this.direction.normalize();
        }

        this.callback();
    },
    reset:function() {
        this.p1.x = Infinity;
        this.p1.y = Infinity;
    },
    debugDraw:function(ctx) {
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#f00';
        ctx.beginPath();
        ctx.moveTo(this.p0.x, this.p0.y);
        ctx.lineTo(this.p1.x, this.p1.y);
        ctx.stroke();
    }
};