site.routers.Main = Backbone.Router.extend({
    routes:{
        '' : 'showMainMenu',
        'projects' : 'showProjects',
        'lab' : 'showLab',
        'writing' : 'showWriting',
        'about' : 'showAbout'
    },
    initialize:function() {
        // check and validate deeplink (before Backbone.history.start())
        if (window.location.hash) {
            var fragment = window.location.hash.split('/')[1];

            if (fragment in this.routes) {
                site.globals.fromDeeplink = true;
                window.location.hash = '/' + fragment; // remove any extra fragments
            }
            else {
                site.globals.fromDeeplink = false;
                window.location.hash = '';
            }
        }
        else {
            site.globals.fromDeeplink = false;
        }

        Backbone.history.start();
    },
    showMainMenu:function() {
        site.events.trigger('router.showMainMenu');
    },
    showProjects:function() {
        site.events.trigger('router.showProjectsView');
    },
    showLab:function() {
        site.events.trigger('router.showLabView');
    },
    showWriting:function() {
        site.events.trigger('router.showWritingView');
    },
    showAbout:function() {
        site.events.trigger('router.showAboutView');
    }
});
