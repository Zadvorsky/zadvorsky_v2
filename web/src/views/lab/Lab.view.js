site.views.Lab = site.views.Content.extend({
    el: $('#lab-view-template').html(),

    initialize:function() {
        this.$section = this.$('.article-container');
        this.views = [];

        this.collection = new site.collections.Pens();
        this.listenTo(this.collection, 'reset', this.renderPens);
        this.collection.fetch({reset:true});
    },
    renderPens:function() {
        this.collection.each(function(model) {
            var view = new site.views.Pen({model:model});
            this.$section.append(view.render().el);
            this.views.push(view);
        }, this);

        this.loadComplete();
    },
    showComplete:function() {
        this.views.forEach(function(view) {
            view.showComplete();
        });
    },
    hideComplete:function() {
        this.views.forEach(function(v) {
            v.hideComplete();
        });
    }
});
