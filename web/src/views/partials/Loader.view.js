site.views.Loader = Backbone.View.extend({
    el:'.loader-container',

    initialize:function() {
        site.events.on('loader.show', this.show, this);
        site.events.on('loader.hide', this.hide, this);
    },
    show:function() {
        this.$el.show();
    },
    hide:function() {
        this.$el.hide();
    }
});
