site.models.Project = Backbone.Model.extend({});
site.collections.Projects = Backbone.Collection.extend({
    model: site.models.Project,
    url: 'data/projects.json'
});
